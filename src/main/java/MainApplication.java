import bean.Animals;
import bean.Zoo;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class MainApplication {
    public static void main(String[] args) {

        EntityManagerFactory entityManagerFactory= Persistence.createEntityManagerFactory("sanyami");


        EntityManager entityManager1=entityManagerFactory.createEntityManager();
        CriteriaBuilder criteriaBuilder1=entityManager1.getCriteriaBuilder();
        CriteriaQuery<Object> criteriaQuery1=criteriaBuilder1.createQuery();
        Root<Animals> from1=criteriaQuery1.from(Animals.class);

        System.out.println("Select all records from Animals");
        CriteriaQuery<Object> select1=criteriaQuery1.select(from1);
        TypedQuery<Object> typedQuery1=entityManager1.createQuery(select1);
        List<Object> resultlist1=typedQuery1.getResultList();
        resultlist1.stream().forEach(System.out::println);



        criteriaQuery1.where(criteriaBuilder1.greaterThan(from1.get("animalAge"),3));
        CriteriaQuery<Object> selectByAge = ((CriteriaQuery<Object>) criteriaQuery1).select(from1);
        TypedQuery<Object> typedQuery3 = entityManager1.createQuery(selectByAge);
        List<Object> list1 = typedQuery3.getResultList();
        list1.stream().forEach(System.out::println);



        EntityManagerFactory entityManagerFactory1= Persistence.createEntityManagerFactory("sanyami");
        EntityManager entityManager=entityManagerFactory1.createEntityManager();
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery<Object[]> criteriaQuery=criteriaBuilder.createQuery(Object[].class);
        Root<Animals> from=criteriaQuery.from(Animals.class);

        System.out.println("Select all records greater than 4 years and group by category");

        select2.where(criteriaBuilder.greaterThan(from.get("age"),4));
        criteriaQuery.multiselect(from.get("animalCategory"),criteriaBuilder.count(from)).groupBy(from.get("animalCategory"));
//            TypedQuery<Object[]> list=entityManager.createQuery(criteriaQuery1);

        System.out.print("category");
        System.out.println("\t Count");
        List<Object[]> list =entityManager1.createQuery(criteriaQuery).getResultList();
        for(Object[] object : list){
            System.out.println(object[0] + "     " + object[1]);

        }

        entityManager.close();
        entityManager1.close();
        entityManagerFactory.close();
        entityManagerFactory1.close();


    }
}
