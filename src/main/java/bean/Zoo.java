package bean;

import javax.persistence.*;
import java.util.List;

@Entity
public class Zoo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int zooId;
    private String zooName;

    @OneToMany(mappedBy = "zoo",cascade = CascadeType.ALL)
    private List<Animals> animalsList;

    public List<Animals> getAnimalsList() {
        return animalsList;
    }

    public void setAnimalsList(List<Animals> animalsList) {
        this.animalsList = animalsList;
    }

    public int getZooId() {
        return zooId;
    }

    public void setZooId(int zooId) {
        this.zooId = zooId;
    }

    public String getZooName() {
        return zooName;
    }

    public void setZooName(String zooName) {
        this.zooName = zooName;
    }

    @Override
    public String toString() {
        return "Zoo{" +
                "zooId=" + zooId +
                ", zooName='" + zooName + '\'' +
                ", animalsList=" + animalsList +
                '}';
    }
}
