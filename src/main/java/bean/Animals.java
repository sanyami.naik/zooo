package bean;

import javax.persistence.*;


@Entity
public class Animals {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int animalId;
    private String animalName;
    private String animalCategory;
    private int animalAge;

    @ManyToOne(cascade = CascadeType.ALL)
    private Zoo zoo;


    public int getAnimalId() {
        return animalId;
    }

    public void setAnimalId(int animalId) {
        this.animalId = animalId;
    }

    public String getAnimalName() {
        return animalName;
    }

    public void setAnimalName(String animalName) {
        this.animalName = animalName;
    }

    public String getAnimalCategory() {
        return animalCategory;
    }

    public void setAnimalCategory(String animalCategory) {
        this.animalCategory = animalCategory;
    }

    public int getAnimalAge() {
        return animalAge;
    }

    public void setAnimalAge(int animalAge) {
        this.animalAge = animalAge;
    }

    @Override
    public String toString() {
        return "Animals{" +
                "animalId=" + animalId +
                ", animalName='" + animalName + '\'' +
                ", animalCategory='" + animalCategory + '\'' +
                ", animalAge=" + animalAge +
                '}';
    }
}
